import {Component, OnInit} from '@angular/core';
import {User} from '../../user';
import {Event} from '../event';
import {SecurityService} from '../../security.service';
import {EventService} from '../event.service';
import {Role} from '../../role.enum';
import {EventType} from '../event-type.enum';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit {

  EventType = EventType;
  Role = Role;
  sessionUser: User | null = null;
  events: Event[] = [];
  event: Event = {
    id: null,
    title: '',
    date: new Date(2019, 8, 30),
    time: '',
    location: '',
    type: '',
  };

  constructor(private securityService: SecurityService, private eventService: EventService, private router: Router) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    this.eventService.getEvents().subscribe(
      e => this.events = e
    );
  }

  createEvent() {
    this.eventService.createEvent(this.event).subscribe(a => this.myMessage());
    this.reset();
  }

  reset() {
    this.event = {
      id: null,
      title: ' ',
      date: new Date(2019, 8, 30),
      time: ' ',
      location: ' ',
      type: ' ',
    };
  }

  myMessage() {
    alert('Event has been created!');
    this.router.navigate(['./event/eventList']);
  }
}
