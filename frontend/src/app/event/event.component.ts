import {Component, OnInit} from '@angular/core';
import {User} from '../user';
import {SecurityService} from '../security.service';
import {Event} from './event';
import {Role} from '../role.enum';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {

  Role = Role;
  sessionUser: User | null = null;
  event: Event = {
    id: null,
    title: '',
    date: new Date(2019, 8, 16),
    time: '',
    location: '',
    type: '',
  };

  constructor(private securityService: SecurityService) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
  }
}
