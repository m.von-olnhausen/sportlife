import {User} from '../user';
import {Event} from './event';

export interface UserStatusEvent {
  attend: string;
  event: Event;
  userEntity: User;
}
