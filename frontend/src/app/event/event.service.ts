import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Event} from './event';
import {HttpClient} from '@angular/common/http';
import {UserStatusEvent} from './userStatusEvent';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  private events = new BehaviorSubject<Event[]>([]);

  event: Event = {
    id: null,
    title: '',
    date: new Date(2019, 8, 17),
    time: '',
    location: '',
    type: '',
  };
  id: number;
  participantList$ = new BehaviorSubject<UserStatusEvent[]|null>(null);
  userStatusList$ = new BehaviorSubject<UserStatusEvent[] | null>(null);

  constructor(private httpClient: HttpClient) {
    this.loadEvents();
  }

  private loadEvents() {
    const requestObservable = this.httpClient.get<Event[]>('/api/event');
    requestObservable.subscribe(e => this.events.next(e));
  }

  getEvents(): Observable<Event[]> {
    return this.events;
  }

  createEvent(event) {
    const requestObservable = this.httpClient.post<Event[]>('/api/createEvent', event);
    return requestObservable.pipe(
      tap(eventList => this.events.next(eventList)));
  }

  attendEvent(id) {
    const requestObservable = this.httpClient.post<Event>('/api/event/' + id + '/attendEvent', this.event);
    requestObservable.subscribe(e => this.event = e);
  }

  declineEvent(id) {
    const requestObservable = this.httpClient.post<Event>('/api/event/' + id + '/declineEvent', this.event);
    requestObservable.subscribe(e => this.event = e);
  }

  viewParticipants(id) {
    const requestObservable = this.httpClient.get<UserStatusEvent[]>('/api/event/' + id + '/eventParticipants');
    requestObservable.subscribe(list => this.participantList$.next(list));
  }

  update(event, id) {
    const requestObservable = this.httpClient.post<Event[]>('/api/event/' + id + '/sendUpdatedEvent', event);
    requestObservable.subscribe(eventList => this.events.next(eventList));
  }

  showStatus() {
    const requestObservable = this.httpClient.get<UserStatusEvent[]>('/api/event/eventList/status');
    requestObservable.subscribe(list => this.userStatusList$.next(list));
  }
}
