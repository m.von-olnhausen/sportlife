import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Event} from '../event';
import {ActivatedRoute} from '@angular/router';
import {UserStatusEvent} from '../userStatusEvent';
import {SecurityService} from '../../security.service';
import {User} from '../../user';
import {Role} from '../../role.enum';
import {EventService} from '../event.service';
import {EventStatus} from '../event-status.enum';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.css']
})
export class EventDetailComponent implements OnInit {

  Role = Role;
  sessionUser: User | null = null;
  participantList: UserStatusEvent[] = [];
  userStatusList: UserStatusEvent[] = [];
  id: number;
  event: Event = {
    id: null,
    title: '',
    date: new Date(2019, 8, 16),
    time: '',
    location: '',
    type: '',
  };
  counterA = 0;
  counterB = 0;
  counterC = 0;

  constructor(private route: ActivatedRoute, private httpClient: HttpClient, private securityService: SecurityService, private eventService: EventService) { }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    this.route.paramMap.subscribe(
     params => {
       this.id = +params.get('id');
       const requestObservable = this.httpClient.get<Event>('/api/event/' + this.id);
       requestObservable.subscribe(event => this.event = event);
     }
   );
    const requestObservable = this.httpClient.get<UserStatusEvent[]>('/api/event/eventList/status');
    requestObservable.subscribe(list => this.userStatusList = list);
    this.eventService.viewParticipants(this.id);
    this.eventService.participantList$.subscribe(list => this.participantList = list);
  }

  attendEvent() {
    this.eventService.attendEvent(this.id);
    this.showStatus();
  }

  declineEvent() {
    this.eventService.declineEvent(this.id);
    this.showStatus();
  }

  showStatus() {
    this.eventService.showStatus();
    this.eventService.userStatusList$.subscribe(list => this.userStatusList = list);
  }

  viewParticipants() {
    this.counterA = 0;
    this.counterB = 0;
    this.counterC = 0;
    for (const p of this.participantList) {
      if (p.attend == EventStatus.Attending && p.userEntity.role == Role.USER) {
        this.counterA++;
      } else if (p.attend == EventStatus.Declined && p.userEntity.role == Role.USER) {
        this.counterB++;
      } else if (p.attend == EventStatus.NoStatus && p.userEntity.role == Role.USER) {
        this.counterC++;
      }
    }
  }
}
