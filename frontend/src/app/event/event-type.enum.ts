export enum EventType {
  COMPETITION = 'COMPETITION',
  TRAINING = 'TRAINING',
  TEAMBUILDING = 'TEAMBUILDING',
}
