import {Component, OnInit} from '@angular/core';
import {Event} from '../event';
import {HttpClient} from '@angular/common/http';
import {User} from '../../user';
import {Role} from '../../role.enum';
import {EventType} from '../event-type.enum';
import {SecurityService} from '../../security.service';
import {ActivatedRoute, Router} from '@angular/router';
import {EventService} from '../event.service';

@Component({
  selector: 'app-update-event',
  templateUrl: './update-event.component.html',
  styleUrls: ['./update-event.component.css']
})
export class UpdateEventComponent implements OnInit {

  Role = Role;
  EventType = EventType;
  sessionUser: User | null = null;
  events: Event[] = [];
  event: Event = {
    id: null,
    title: '',
    date: new Date(2019, 8, 16),
    time: '',
    location: '',
    type: '',
  };
  id: number;
  public message = 'Event has been updated!';

  constructor(private route: ActivatedRoute, private httpClient: HttpClient, private securityService: SecurityService, private router: Router, private eventService: EventService) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    this.route.paramMap.subscribe(
      params => {
        this.id = +params.get('id');
        console.log(this.id);
        const requestObservable = this.httpClient.get<Event>('/api/event/' + this.id + '/updateEvent');
        requestObservable.subscribe(event => this.event = event);
      }
    );
    this.eventService.getEvents().subscribe(
      e => this.events = e
    );
  }

  update() {
    this.eventService.update(this.event, this.id);
  }

  myMessage() {
    alert(this.message);
    this.router.navigate(['./event/' + this.id]);
  }
}
