import {Component, OnInit} from '@angular/core';
import {User} from '../../user';
import {Event} from '../event';
import {SecurityService} from '../../security.service';
import {EventService} from '../event.service';
import {Role} from '../../role.enum';
import {UserStatusEvent} from '../userStatusEvent';
import {HttpClient} from '@angular/common/http';
import {EventType} from '../event-type.enum';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {

  Role = Role;
  EventType = EventType;
  sessionUser: User | null = null;
  events: Event[] = [];
  event: Event = {
    id: null,
    title: '',
    date: new Date(2019, 8, 16),
    time: '',
    location: '',
    type: '',
  };
  userStatusList: UserStatusEvent[] = [];

  constructor(private securityService: SecurityService, private eventService: EventService, private  httpClient: HttpClient) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    this.eventService.getEvents().subscribe(
      e => this.events = e
    );
    const requestObservable = this.httpClient.get<UserStatusEvent[]>('/api/event/eventList/status');
    requestObservable.subscribe(list => this.userStatusList = list );
  }
}
