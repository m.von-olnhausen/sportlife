export enum EventStatus {
  Attending = 'Attending',
  Declined = 'Declined',
  NoStatus = 'No status',
}
