import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EventComponent} from './event/event.component';
import {LogInRouteGuard} from './LogInRouteGuard';
import {CreateEventComponent} from './event/create-event/create-event.component';
import {AdminRouteGuard} from './AdminRouteGuard';
import {EventDetailComponent} from './event/event-detail/event-detail.component';
import {EventListComponent} from './event/event-list/event-list.component';
import {UserListComponent} from './user-list/user-list.component';
import {ProfilComponent} from './profil/profil.component';
import {MessageComponent} from './message/message.component';
import {UpdateEventComponent} from './event/update-event/update-event.component';

const routes: Routes = [
  {path: 'event', component: EventComponent, canActivate: [LogInRouteGuard], children: [
      {path: 'eventList', component: EventListComponent, canActivate: [LogInRouteGuard]},
      {path: 'createEvent', component: CreateEventComponent, canActivate: [AdminRouteGuard]},
      {path: ':id', component: EventDetailComponent, canActivate: [LogInRouteGuard]},
      {path: ':id/updateEvent', component: UpdateEventComponent, canActivate: [AdminRouteGuard]},
    ]
  },
  {path: 'users', component: UserListComponent, canActivate: [LogInRouteGuard]},
  {path: 'profile', component: ProfilComponent, canActivate: [LogInRouteGuard]},
  {path: 'message', component:MessageComponent, canActivate: [LogInRouteGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
