import {Role} from './role.enum';

export interface User {
  number: number;
  bday: Date;
  position: string;
  username: string;
  role: Role;
  id: number;
  firstname: string;
  lastname: string;
  email: string;
  phone: string;
}
