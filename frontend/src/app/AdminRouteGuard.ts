import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {SecurityService} from './security.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class AdminRouteGuard implements CanActivate {

  constructor(private auth: SecurityService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.auth.getSessionUser().pipe(
      map(u => {
        if (u !== null) {
          if (u.role == 'ADMIN') {
            return true;
          }
        }
        return false;
      })
    );
  }
}
