import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {User} from './user';
import {SecurityService} from './security.service';
import {Role} from './role.enum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  @ViewChild('navBurger', { static: false }) navBurger: ElementRef;
  @ViewChild('navMenu', { static: false }) navMenu: ElementRef;

  toggleNavbar() {
    this.navBurger.nativeElement.classList.toggle('is-active');
    this.navMenu.nativeElement.classList.toggle('is-active');
  }

  Role = Role;
  title = 'frontend';
  sessionUser: User | null = null;

  constructor(private securityService: SecurityService) {
  }

  ngOnInit(): void {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
  }
}
