import {Component, OnInit} from '@angular/core';
import {MessageService} from './message.service';
import {SecurityService} from '../security.service';
import {User} from '../user';
import {Message} from './message';
import {Role} from '../role.enum';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  Role = Role;
  sessionUser: User | null = null;
  messages: Message[] = [];
  message: Message = {
    message: '',
    date: new Date(2019, 8, 30),
    username: '',
  };

  constructor(private messageService: MessageService, private securityService: SecurityService) { }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    this.messageService.getMessages().subscribe(
      mL => this.messages = mL
    );
  }

  createMessage() {
    this.messageService.createMessage(this.message);
    this.reset();
  }

  reset() {
    this.message = {
      message: ' ',
      date: new Date(2019, 8, 30),
      username: '',
    };
  }

}
