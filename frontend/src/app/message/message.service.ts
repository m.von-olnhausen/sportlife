import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Message} from './message';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  private messages = new BehaviorSubject<Message[]>([]);

  constructor(private httpClient: HttpClient) { this.loadMessages(); }

  loadMessages() {
    const requestObservable = this.httpClient.get<Message[]>('/api/messages');
    requestObservable.subscribe(u => this.messages.next(u));
  }

  getMessages(): Observable<Message[]> {
    return this.messages;
  }

  createMessage(message) {
    const requestObservable = this.httpClient.post<Message[]>('/api/createMessage', message);
    requestObservable.subscribe(messageList => this.messages.next(messageList));
  }
}
