export interface Message {
  message: string;
  date: Date;
  username: string;
}
