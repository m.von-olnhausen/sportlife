import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from './user';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private users = new BehaviorSubject<User[]>([]);
  user: User = {
    bday: new Date(2019, 8, 16),
    position: '',
    number: null,
    username: '',
    role: null,
    id: null,
    firstname: '',
    lastname: '',
    email: '',
    phone: '',
  };

  constructor(private  httpClient: HttpClient) { this.loadUsers(); }

  loadUsers() {
    const requestObservable = this.httpClient.get<User[]>('/api/users');
    requestObservable.subscribe(u => this.users.next(u));
  }

  getUsers(): Observable<User[]> {
    return this.users;
  }

  appointAdmin(user: User) {
    const requestObservable = this.httpClient.post<User[]>('/api/users/appointAdmin/' + user.id, null);
    requestObservable.subscribe(u => this.users.next(u));
  }

  confirmGuest(user: User) {
    const requestObservable = this.httpClient.post<User[]>('/api/users/confirmGuest/' + user.id, null);
    requestObservable.subscribe(u => this.users.next(u));
  }
}
