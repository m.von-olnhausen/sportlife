import {Component, OnInit} from '@angular/core';
import {User} from '../user';
import {UserService} from '../user.service';
import {SecurityService} from '../security.service';
import {Role} from '../role.enum';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  sessionUser: User | null = null;
  users: User[] = [];
  Role = Role;

  constructor(private securityService: SecurityService, private userService: UserService) { }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    this.userService.getUsers().subscribe(
      uL => this.users = uL
    );
  }

  appointAdmin(user: User) {
    this.userService.appointAdmin(user);
  }

  confirmGuest(user: User) {
    this.userService.confirmGuest(user);
  }
}
