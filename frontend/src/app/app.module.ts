import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AgmCoreModule} from '@agm/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SessionUserComponent} from './session-user/session-user.component';
import {HttpClientModule} from '@angular/common/http';
import {LoginComponent} from './login/login.component';
import {FormsModule} from '@angular/forms';
import {EventComponent} from './event/event.component';
import {SecurityService} from './security.service';
import {LogInRouteGuard} from './LogInRouteGuard';
import {AdminRouteGuard} from './AdminRouteGuard';
import {CreateEventComponent} from './event/create-event/create-event.component';
import {EventDetailComponent} from './event/event-detail/event-detail.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {EventListComponent} from './event/event-list/event-list.component';
import {UserListComponent} from './user-list/user-list.component';
import {ProfilComponent} from './profil/profil.component';
import {MessageComponent} from './message/message.component';
import {UpdateEventComponent} from './event/update-event/update-event.component';

@NgModule({
  declarations: [
    AppComponent,
    SessionUserComponent,
    LoginComponent,
    EventComponent,
    CreateEventComponent,
    EventDetailComponent,
    EventListComponent,
    UserListComponent,
    ProfilComponent,
    MessageComponent,
    UpdateEventComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    FontAwesomeModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAA2XmA3pGqhKVMpO1Zc2DrMTrY4daYBbM'
    })
  ],
  providers: [SecurityService, LogInRouteGuard, AdminRouteGuard],
  bootstrap: [AppComponent]
})
export class AppModule {
}
