import {Component, OnInit} from '@angular/core';
import {UserService} from '../user.service';
import {SecurityService} from '../security.service';
import {User} from '../user';
import {Role} from '../role.enum';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  sessionUser: User | null = null;
  users: User[] = [];
  Role = Role;

  constructor(private userService: UserService, private securityService: SecurityService) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    this.userService.getUsers().subscribe(
      uL => this.users = uL
    );
  }
}
