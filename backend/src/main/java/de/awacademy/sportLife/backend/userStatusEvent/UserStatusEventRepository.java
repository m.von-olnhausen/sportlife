package de.awacademy.sportLife.backend.userStatusEvent;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserStatusEventRepository extends JpaRepository<UserStatusEvent, Integer> {
    UserStatusEvent findByUserEntityIdAndEventId(int userId, int eventId);

    List<UserStatusEvent> findByEventId(int eventId);

    List<UserStatusEvent> findByUserEntityId(int userId);
}
