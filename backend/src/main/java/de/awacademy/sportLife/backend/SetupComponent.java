package de.awacademy.sportLife.backend;

import de.awacademy.sportLife.backend.event.Event;
import de.awacademy.sportLife.backend.event.EventService;
import de.awacademy.sportLife.backend.user.Role;
import de.awacademy.sportLife.backend.user.UserEntity;
import de.awacademy.sportLife.backend.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDate;

@Component
public class SetupComponent {

    private UserService userService;
    private EventService eventService;

    @Autowired
    public SetupComponent(UserService userService, EventService eventService) {
        this.userService = userService;
        this.eventService = eventService;
    }

    @EventListener
    @Transactional
    public void handleApplicationReady(ApplicationReadyEvent event) {

        //User
        if (!userService.usernameExists("admin")) {
            UserEntity userEntity = userService.register("admin", "admin", Role.ADMIN, "Coach", LocalDate.of(1973, 11, 23), 0, "Trainer", "Ludwig", "trainer.ludwig69@gmail.com", "0153 123456");
        }
        if (!userService.usernameExists("user")) {
            UserEntity userEntity = userService.register("user", "user", Role.USER, "Offence", LocalDate.of(1994, 3, 7), 10, "Dr.", "Hollywood", "e@mail.com", "0800 121212");
        }
        if (!userService.usernameExists("guest")) {
            UserEntity userEntity = userService.register("guest", "guest", Role.GUEST, "Defence", LocalDate.of(1989, 7, 30), 5, "Dr.Dr.", "Sunshine", "web.de@gmx.de", "666 9191919");
        }
        if (!userService.usernameExists("user1")) {
            UserEntity userEntity = userService.register("user1", "user1", Role.USER, "Offence", LocalDate.of(1995, 2, 5), 11, "Michael", "Scott", "asd@dings.de", "0800 1212132");
        }
        if (!userService.usernameExists("user2")) {
            UserEntity userEntity = userService.register("user2", "user2", Role.USER, "Defence", LocalDate.of(1996, 9, 21), 1, "Jon", "Snow", "asdasd@asfas.com", "0800 1212412");
        }
        if (!userService.usernameExists("user3")) {
            UserEntity userEntity = userService.register("user3", "user3", Role.USER, "Offence", LocalDate.of(1997, 8, 23), 2, "Sheldon", "Cooper", "a@mail.com", "0800 1215212");
        }
        if (!userService.usernameExists("user4")) {
            UserEntity userEntity = userService.register("user4", "user4", Role.USER, "Defence", LocalDate.of(1998, 8, 31), 4, "Ted", "Mosby", "b@mail.com", "0800 1212162");
        }
        if (!userService.usernameExists("user5")) {
            UserEntity userEntity = userService.register("user5", "user5", Role.USER, "Offence", LocalDate.of(1999, 3, 4), 15, "Harvey", "Specter", "c@mail.com", "0800 1121212");
        }
        if (!userService.usernameExists("user6")) {
            UserEntity userEntity = userService.register("user6", "user6", Role.USER, "Defence", LocalDate.of(1993, 4, 1), 23, "Raylan", "Givens", "d@mail.com", "0800 1213212");
        }
        if (!userService.usernameExists("user7")) {
            UserEntity userEntity = userService.register("user7", "user7", Role.USER, "Offence", LocalDate.of(1994, 1, 26), 99, "Phil", "Dunphy", "f@mail.com", "0800 1212912");
        }
        if (!userService.usernameExists("user8")) {
            UserEntity userEntity = userService.register("user8", "user8", Role.USER, "Defence", LocalDate.of(1994, 12, 7), 12, "Jackson", "Teller", "g@mail.com", "0800 1212812");
        }
        if (!userService.usernameExists("user9")) {
            UserEntity userEntity = userService.register("user9", "user9", Role.USER, "Offence", LocalDate.of(1994, 12, 12), 7, "Dexter", "Morgan", "h@mail.com", "0800 1212712");
        }
        if (!userService.usernameExists("user10")) {
            UserEntity userEntity = userService.register("user10", "user10", Role.USER, "Defence", LocalDate.of(1995, 8, 19), 33, "Daryl", "Dixon", "j@mail.com", "0800 1261212");
        }
        if (!userService.usernameExists("user11")) {
            UserEntity userEntity = userService.register("user11", "user11", Role.USER, "Offence", LocalDate.of(1996, 10, 29), 43, "Regina", "Phalange", "i@mail.com", "0800 1212912");
        }

        //Events
        /*if (!eventService.eventExists("Home vs. Smells Like Team Spirit")) {
            Event event1 = eventService.create("Home vs. Smells Like Team Spirit", LocalDate.of(2019, 9, 2), "Allianz Arena", "COMPETITION");
        }
        if (!eventService.eventExists("Away vs. Our Uniforms Match")) {
            Event event1 = eventService.create("Away vs. Our Uniforms Match", LocalDate.of(2019, 9, 8), "Veltins Arena", "COMPETITION");
        }
        if (!eventService.eventExists("Home vs. Knock, knock! Who’s there?")) {
            Event event1 = eventService.create("Home vs. Knock, knock! Who’s there?", LocalDate.of(2019, 9, 13), "Allianz Arena", "COMPETITION");
        }
        if (!eventService.eventExists("Away vs. Cereal Killers")) {
            Event event1 = eventService.create("Away vs. Cereal Killers", LocalDate.of(2019, 9, 21), "Olympia Stadium", "COMPETITION");
        }
        if (!eventService.eventExists("Home vs. Life’s A Pitch")) {
            Event event1 = eventService.create("Home vs. Life’s A Pitch", LocalDate.of(2019, 9, 25), "Allianz Arena", "COMPETITION");
        }
        if (!eventService.eventExists("Training Offence")) {
            Event event1 = eventService.create("Training Offence", LocalDate.of(2019, 9, 23), "Training Grounds", "TRAINING");
        }
        if (!eventService.eventExists("Training Defence")) {
            Event event1 = eventService.create("Training Defence", LocalDate.of(2019, 9, 19), "Training Grounds", "TRAINING");
        }
        if (!eventService.eventExists("Training whole Squad")) {
            Event event1 = eventService.create("Training whole Squad", LocalDate.of(2019, 9, 4), "Training Grounds", "TRAINING");
        }
        if (!eventService.eventExists("Bowling")) {
            Event event1 = eventService.create("Bowling", LocalDate.of(2019, 9, 3), "Bowling Center", "TEAMBUILDING");
        }
        if (!eventService.eventExists("Pub Crawl")) {
            Event event1 = eventService.create("Pub Crawl", LocalDate.of(2019, 9, 17), "City Center", "TEAMBUILDING");
        }*/

    }
}
