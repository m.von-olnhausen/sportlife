package de.awacademy.sportLife.backend.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class UserService {
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public boolean usernameExists(String username) {
        return userRepository.existsByUsernameIgnoreCase(username);
    }

    public UserEntity register(String username, String password, Role role, String position, LocalDate bday, int number, String firstname, String lastname, String email, String phone) {
        String encodedPassword = passwordEncoder.encode(password);
        return userRepository.save(new UserEntity(username, encodedPassword, role, bday, position, number, firstname, lastname, email, phone));
    }
}
