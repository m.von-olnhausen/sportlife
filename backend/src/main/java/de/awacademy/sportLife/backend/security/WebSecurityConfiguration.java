package de.awacademy.sportLife.backend.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic();
        http.authorizeRequests()
                .antMatchers("/api/event").hasAnyAuthority("ADMIN", "USER")
                .antMatchers("/api/createEvent").hasAuthority("ADMIN")
                .antMatchers("/api/event/{id}").hasAnyAuthority("ADMIN", "USER")
                .antMatchers("/api/event/{id}/eventParticipants").hasAuthority("ADMIN")
                .antMatchers("/api/event/{id}/attendEvent").hasAuthority("USER")
                .antMatchers("/api/event/{id}/declineEvent").hasAuthority("USER")
                .antMatchers("/api/users").hasAnyAuthority("ADMIN", "USER")
                .antMatchers("/api/users/appointAdmin/{id}").hasAuthority("ADMIN")
                .antMatchers("/api/users/confirmGuest/{id}").hasAuthority("ADMIN")
                .antMatchers("/api/event/eventList/status").hasAnyAuthority("ADMIN", "USER")
                .antMatchers("/api/messages").hasAnyAuthority("ADMIN", "USER")
                .antMatchers("/api/createMessage").hasAnyAuthority("ADMIN", "USER")
                .antMatchers("/api/event/{id}/updateEvent").hasAuthority("ADMIN")
                .antMatchers("/api/event/{id}/sendUpdatedEvent").hasAuthority("ADMIN")
                .anyRequest().authenticated();

        http.exceptionHandling()
                .authenticationEntryPoint(new Http403ForbiddenEntryPoint());

        http.logout()
                .logoutUrl("/api/logout")
                .logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler());

        http.csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
    }
}
