package de.awacademy.sportLife.backend.user;

import java.time.LocalDate;

public class UserDTO {

    private String username;

    private Role role;

    private int id;

    private LocalDate bday;

    private String position;

    private int number;

    private String firstname;

    private String lastname;

    private String email;

    private String phone;

    public UserDTO(String username, Role role, int id, LocalDate bday, String position, int number, String firstname, String lastname, String email, String phone) {
        this.username = username;
        this.role = role;
        this.id = id;
        this.bday = bday;
        this.position = position;
        this.number = number;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public Role getRole() {
        return role;
    }

    public int getId() {
        return id;
    }

    public LocalDate getBday() {
        return bday;
    }

    public String getPosition() {
        return position;
    }

    public int getNumber() {
        return number;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }
}
