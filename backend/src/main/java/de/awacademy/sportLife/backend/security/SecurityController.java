package de.awacademy.sportLife.backend.security;

import de.awacademy.sportLife.backend.user.UserDTO;
import de.awacademy.sportLife.backend.user.UserEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecurityController {

    @GetMapping("/api/sessionUser")
    public UserDTO sessionUser(@ModelAttribute("sessionUser") UserEntity userEntity) {
        return new UserDTO(userEntity.getUsername(), userEntity.getRole(), userEntity.getId(), userEntity.getBday(), userEntity.getPosition(), userEntity.getNumber(), userEntity.getFirstname(), userEntity.getLastname(), userEntity.getEmail(), userEntity.getPhone());
    }
}
