package de.awacademy.sportLife.backend.user;

import de.awacademy.sportLife.backend.userStatusEvent.UserStatusEvent;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
public class UserEntity {

    //Fields
    @Id
    @GeneratedValue
    private int id;

    private String username;

    private String password;

    @Enumerated(EnumType.STRING)
    private Role role;

    @OneToMany(mappedBy = "userEntity")
    private Set<UserStatusEvent> userStatusEvents;

    private LocalDate bday;

    private String position;

    private int number;

    private String firstname;

    private String lastname;

    private String email;

    private String phone;


    //Constructor
    public UserEntity(String username, String password, Role role, LocalDate bday, String position, int number, String firstname, String lastname, String email, String phone) {
        this.username = username;
        this.password = password;
        this.role = role;
        this.bday = bday;
        this.position = position;
        this.number = number;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.phone = phone;
    }

    public UserEntity() {
    }

    //Getter
    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Role getRole() {
        return role;
    }

    public LocalDate getBday() {
        return bday;
    }

    public String getPosition() {
        return position;
    }

    public int getNumber() {
        return number;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    //Setter
    public void setRole(Role role) {
        this.role = role;
    }
}
