package de.awacademy.sportLife.backend.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.time.LocalDate;

@Service
public class EventService {

    private EventRepository eventRepository;

    @Autowired
    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public boolean eventExists(String title) {
        return eventRepository.existsByTitleIgnoreCase(title);
    }

    public Event create(String title, LocalDate date, String time, String location, String type) {
        return eventRepository.save(new Event(title, date, time, location, type));
    }
}
