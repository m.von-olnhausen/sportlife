package de.awacademy.sportLife.backend.message;


import java.time.LocalDateTime;

public class MessageDTO {

    private String message;

    private String username;

    private LocalDateTime dateTime;

    public MessageDTO(String message, String username, LocalDateTime dateTime) {
        this.message = message;
        this.username = username;
        this.dateTime = dateTime;
    }

    public MessageDTO() {
    }

    public String getMessage() {
        return message;
    }

    public String getUsername() {
        return username;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }
}
