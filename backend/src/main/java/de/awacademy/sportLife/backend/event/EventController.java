package de.awacademy.sportLife.backend.event;

import de.awacademy.sportLife.backend.user.UserEntity;
import de.awacademy.sportLife.backend.user.UserRepository;
import de.awacademy.sportLife.backend.userStatusEvent.UserStatusEvent;
import de.awacademy.sportLife.backend.userStatusEvent.UserStatusEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
public class EventController {

    private EventRepository eventRepository;
    private UserRepository userRepository;
    private UserStatusEventRepository userStatusEventRepository;

    @Autowired
    public EventController(EventRepository eventRepository, UserRepository userRepository, UserStatusEventRepository userStatusEventRepository) {
        this.eventRepository = eventRepository;
        this.userRepository = userRepository;
        this.userStatusEventRepository = userStatusEventRepository;
    }

    @GetMapping("/api/event")
    public List<EventDTO> showEvents() {
        List<EventDTO> eventList = new LinkedList<>();
        for (Event event : eventRepository.findAllByOrderByDateAsc())
        {
            eventList.add(new EventDTO(event.getTitle(), event.getDate(), event.getTime(), event.getLocation(), event.getType(), event.getId()));
        }
        return eventList;
    }

    @PostMapping("/api/createEvent")
    public List<EventDTO> createEvent(@RequestBody EventDTO eventDTO) {
        Event event = new Event(eventDTO.getTitle(), eventDTO.getDate(), eventDTO.getTime(), eventDTO.getLocation(), eventDTO.getType());
        eventRepository.save(event);
        List<UserEntity> userList = userRepository.findAll();
        for (UserEntity userEntity : userList) {
            UserStatusEvent u = new UserStatusEvent(userEntity, event, "No status");
            userStatusEventRepository.save(u);
        }
        return showEvents();
    }

    @GetMapping("/api/event/{id}/updateEvent")
    public EventDTO updateEvent(@PathVariable int id) {
        Event event = eventRepository.findById(id);
        EventDTO eventDTO = new EventDTO(event.getTitle(), event.getDate(), event.getTime(), event.getLocation(), event.getType(), event.getId());
        return eventDTO;
    }

    @PostMapping("/api/event/{id}/sendUpdatedEvent")
    public List<EventDTO> update(@ModelAttribute("sessionUser") UserEntity sessionUser, @PathVariable int id,
                                 @RequestBody EventDTO eventDTO) {
        Event event = eventRepository.findById(id);
        event.setTitle(eventDTO.getTitle());
        event.setDate(eventDTO.getDate());
        event.setTime(eventDTO.getTime());
        event.setLocation(eventDTO.getLocation());
        event.setType(eventDTO.getType());
        eventRepository.save(event);
        return showEvents();
    }


    @PostMapping("/api/event/{id}/attendEvent")
    public Event attendEvent(@ModelAttribute("sessionUser") UserEntity sessionUser, @PathVariable int id) {
        UserStatusEvent u = userStatusEventRepository.findByUserEntityIdAndEventId(sessionUser.getId(), id);
        u.setAttend("Attending");
        userStatusEventRepository.save(u);
        return getId(id);
   }

    @PostMapping("/api/event/{id}/declineEvent")
    public Event declineEvent(@ModelAttribute("sessionUser") UserEntity sessionUser, @PathVariable int id) {
        UserStatusEvent u = userStatusEventRepository.findByUserEntityIdAndEventId(sessionUser.getId(), id);
        u.setAttend("Declined");
        userStatusEventRepository.save(u);
        return getId(id);
    }

    @GetMapping("/api/event/{id}")
    public Event getId(@PathVariable int id) {
        Event event = eventRepository.findById(id);
        return event;
    }

    @GetMapping("/api/event/{id}/eventParticipants")
    public List<UserStatusEvent> showParticipants(@PathVariable int id) {
        List<UserStatusEvent> participantList = userStatusEventRepository.findByEventId(id);
        return participantList;
    }

    @GetMapping("/api/event/eventList/status")
    public List<UserStatusEvent> showStatus(@ModelAttribute("sessionUser") UserEntity sessionUser) {
        List<UserStatusEvent> userStatusList = userStatusEventRepository.findByUserEntityId(sessionUser.getId());
        return userStatusList;
    }
}
