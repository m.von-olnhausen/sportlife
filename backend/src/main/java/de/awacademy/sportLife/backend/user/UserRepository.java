package de.awacademy.sportLife.backend.user;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Integer> {
    Optional<UserEntity> findByUsername(String username);

    boolean existsByUsernameIgnoreCase(String username);

    List<UserEntity> findAll();

    UserEntity findById(int id);
}
