package de.awacademy.sportLife.backend.event;

import java.sql.Time;
import java.time.LocalDate;

public class EventDTO {

    private String title;

    private LocalDate date;

    private String time;

    private String location;

    private String type;

    private int id;

    public EventDTO(String title, LocalDate date, String time, String location, String type, int id) {
        this.title = title;
        this.date = date;
        this.time = time;
        this.location = location;
        this.type = type;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getTime () { return time;}

    public String getLocation() {
        return location;
    }

    public String getType() {
        return type;
    }

    public int getId() {
        return id;
    }
}
