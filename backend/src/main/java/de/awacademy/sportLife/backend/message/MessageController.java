package de.awacademy.sportLife.backend.message;

import de.awacademy.sportLife.backend.user.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@RestController
public class MessageController {

    private MessageRepository messageRepository;

    @Autowired
    public MessageController(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @GetMapping("/api/messages")
    public List<MessageDTO> showMessages() {
        List<MessageDTO> messageList = new LinkedList<>();
        for (Message message : messageRepository.findAllByOrderByDateTimeAsc()) {
            messageList.add(new MessageDTO(message.getMessage(), message.getUsername(), message.getDateTime()));
        }
        return messageList;
    }


    @PostMapping("/api/createMessage")
    public List<MessageDTO> createMessage(@RequestBody MessageDTO messageDTO, @ModelAttribute("sessionUser") UserEntity sessionUser) {
        Message message = new Message(messageDTO.getMessage(), LocalDateTime.now(), sessionUser.getUsername());
        messageRepository.save(message);
        return showMessages();
    }
}
