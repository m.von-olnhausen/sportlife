package de.awacademy.sportLife.backend.userStatusEvent;

import de.awacademy.sportLife.backend.event.Event;
import de.awacademy.sportLife.backend.user.UserEntity;

import javax.persistence.*;

@Entity
public class UserStatusEvent {

    @Id
    @GeneratedValue
    private int id;

    @ManyToOne
    @JoinColumn(name = "userEntity_id")
    private UserEntity userEntity;

    @ManyToOne
    @JoinColumn(name = "event_id")
    private Event event;

    private String attend;

    public UserStatusEvent(UserEntity userEntity, Event event, String attend) {
        this.userEntity = userEntity;
        this.event = event;
        this.attend = attend;
    }

    public UserStatusEvent() {
    }

    public int getId() {
        return id;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public Event getEvent() {
        return event;
    }

    public String getAttend() {
        return attend;
    }

    public void setAttend(String attend) {
        this.attend = attend;
    }
}
