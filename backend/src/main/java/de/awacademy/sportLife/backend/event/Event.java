package de.awacademy.sportLife.backend.event;

import de.awacademy.sportLife.backend.userStatusEvent.UserStatusEvent;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.sql.Time;
import java.time.LocalDate;
import java.util.Set;

@Entity
public class Event {

    @Id
    @GeneratedValue
    private int id;

    private String title;

    private LocalDate date;

    private String time;

    private String location;

    private String type;

    @OneToMany(mappedBy = "event")
    private Set<UserStatusEvent> userStatusEvents;

    //Constructors
    public Event() {
    }

    public Event(String title, LocalDate date, String time, String location, String type) {
        this.title = title;
        this.date = date;
        this.time = time;
        this.location = location;
        this.type = type;
    }

    //Getter
    public String getTitle() {
        return title;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getTime () {return time;}

    public String getLocation() {
        return location;
    }

    public String getType() {
        return type;
    }

    public int getId() {
        return id;
    }

    //Setter

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setTime (String time) { this.time = time;}

    public void setLocation(String location) {
        this.location = location;
    }

    public void setType(String type) {
        this.type = type;
    }
}
