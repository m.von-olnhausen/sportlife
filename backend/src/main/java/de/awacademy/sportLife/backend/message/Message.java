package de.awacademy.sportLife.backend.message;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
public class Message {

    @Id
    @GeneratedValue
    private int id;

    private String message;

    private LocalDateTime dateTime;

    private String username;

    public Message(String message, LocalDateTime dateTime, String username) {
        this.message = message;
        this.dateTime = dateTime;
        this.username = username;
    }


    public Message() {
    }

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public String getUsername() {
        return username;
    }
}
