package de.awacademy.sportLife.backend.event;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends CrudRepository<Event, Integer> {
    List<Event> findAllByOrderByDateAsc();

    Event findById(int id);

    boolean existsByTitleIgnoreCase(String title);
    //Optional<Event> findById();
}
