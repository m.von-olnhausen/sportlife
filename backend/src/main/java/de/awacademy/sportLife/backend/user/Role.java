package de.awacademy.sportLife.backend.user;

public enum Role {
    ADMIN, USER, GUEST
}
