package de.awacademy.sportLife.backend.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
public class UserController {

    private UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/api/users")
    public List<UserDTO> showUsers() {
        List<UserDTO> userList = new LinkedList<>();

        for (UserEntity user : userRepository.findAll()) {
            userList.add(new UserDTO(user.getUsername(), user.getRole(), user.getId(), user.getBday(), user.getPosition(), user.getNumber(), user.getFirstname(), user.getLastname(), user.getEmail(), user.getPhone()));
        }
        return userList;
    }

    @PostMapping("/api/users/appointAdmin/{id}")
    public List<UserDTO> appointAdmin(@ModelAttribute("sessionUser") UserEntity sessionUser, @PathVariable int id) {
        UserEntity u = userRepository.findById(id);
        u.setRole(Role.ADMIN);
        userRepository.save(u);
        return showUsers();
    }

    @PostMapping("/api/users/confirmGuest/{id}")
    public List<UserDTO> confirmGuest(@ModelAttribute("sessionUser") UserEntity sessionUser, @PathVariable int id) {
        UserEntity u = userRepository.findById(id);
        u.setRole(Role.USER);
        userRepository.save(u);
        return  showUsers();
    }
}
